## ReactJS, Redux, create-react-app

elinvar.de 

### Case Study Shopping Cart 

**Task:**

Create an application Shopping Cart. On Products page you see list of products dowloaded from server. 
Each product can have an image, name, description, items left and price. You can add item to your cart. 
On the top of the page there is navigation  bar  with  links:  All  Products,  Top-5  Viewed  and  shopping 
cart  icon  with badge that displays amount of items in the cart. When you click on shopping cart you go to cart page where you can see all 
added products in a list view, you can change  quantity  of  each  product.  summary  on  the  page  calculates  price  of  all 
products in the cart.

### Products API
```json
{
  "productID": 6,
  "name": "Grandma's Boysenberry Spread",
  "description": "Some description of product, does not matter",
  "unitPrice": 25,
  "unitsInStock": 120,
  "image": "http://lorempixel.com/400/200/technics/"
}

```
### Screenshoot
[image] 
### Spent time

3,45 hours
 
### Run project 

```sh
npm i
npm start
```

### Developer
[@nickkireev]

[image]: <https://imgur.com/a/81bG4wx>
[@nickkireev]: <https://bitbucket.org/nickkireev>
