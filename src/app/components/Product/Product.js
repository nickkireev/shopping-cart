import React, {Component} from 'react';
import './Product.css';

export default class Product extends Component {
    render() {
        const {image, name, description, productID} = this.props;

        return (
            <div className="product">
                <img alt={name} className="image" src={image}/>
                <div className="name">{name}</div>
                <p className="description">{description}</p>
                <button title="Add to cart" className="button" onClick={() => this.props.onAdd(productID)}>+</button>
            </div>
        );
    }
}