import React, {Component} from 'react';
import './Products.css';
import Product from '../../containers/Product';

export default class Products extends Component {
    render() {
        const {products, maxNumber} = this.props;
        let newProducts = products;
        if (maxNumber) {
            newProducts = products.slice(0, maxNumber);
        }

        return (
            <div className="products">
                {newProducts.map(product => <Product key={`product-${product.productID}`} {...product}/>)}
            </div>
        );
    }
}