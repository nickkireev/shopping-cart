import React, {Component} from 'react';
import './App.css';
import Products from '../../containers/Products';
import Cart from '../../containers/Cart';

class App extends Component {
    constructor(props) {
        super(props);
        if (typeof props.onMount === 'function') {
            props.onMount();
        }
    }

    state = {
        selected: 0,
    }

    select(selected) {
        this.setState({ selected })
    }
    render() {
        const {total} = this.props;
        const {selected} = this.state;
        return (
            <div className="App">
                <header className="header">
                    <h2 className={`title ${selected === 0 && 'selected'}`} onClick={() => this.select(0)}>Home</h2>
                    <h2 className={`title ${selected === 1 && 'selected'}`} onClick={() => this.select(1)}>Top-5 products</h2>
                    <h2 className={`title ${selected === 2 && 'selected'}`}  onClick={() => this.select(2)}>Your cart<span className="total">{total}</span></h2>
                </header>
                {selected === 0 && <Products />}
                {selected === 1 && <Products maxNumber={5}/>}
                {selected === 2 && <Cart />}
            </div>
        );
    }
}

export default App;
