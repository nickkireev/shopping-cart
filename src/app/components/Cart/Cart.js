import React, {Component} from 'react';
import SmallProduct from '../../containers/SmallProduct';
import './Cart.css';

export default class Cart extends Component {
    render() {
        const {selectedProducts, total} = this.props;

        return (
            <div>
                <div className="cart-wrapper">
                    {selectedProducts.map(prod => <SmallProduct key={`small-${prod.productID}`} {...prod}/>)}
                </div>
                <div className="total-wrapper">
                    <hr style={{ color: '#ffffff', width: '100%'}}/>
                    {total !== 0 && <div>Total Price: {total}$</div>}
                    {total === 0 && <div>Your cart is empty</div>}
                </div>
            </div>
        );
    }
}