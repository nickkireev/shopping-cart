import React, {Component} from 'react';
import './SmallProduct.css';

export default class SmallProduct extends Component {
    render() {
        const {
            name,
            unitPrice,
            unitsInStock,
            image,
            count,
        } = this.props;

        return (
            <div className="cart-item">
                <div className="cart-image"><img  className="img" alt={name} src={image}/></div>
                <div className="cart-name-section">
                    <div className="cart-name">{name}</div>
                    <div className="cart-itemsLeft">items left in stock: {unitsInStock}</div>
                </div>
                <div className="cart-price">price: {unitPrice}$</div>
                <div className="cart-number-section">
                    <input
                        className="cart-number"
                        type="number" value={count}
                        onChange={(event) => this.props.changeQuantity(parseInt(event.target.value, 10))}
                    />
                    <button className="remove-btn" onClick={() => this.props.changeQuantity(0)}>x</button>
                </div>
            </div>
        );
    }
}