import {
    FETCH_PRODUCTS,
    FETCH_PRODUCTS_SUCCESS,
    FETCH_PRODUCTS_ERROR,
    ADD_TO_CART,
    REMOVE_FROM_CART,
    CHANGE_QUANTITY,
} from '../actions';

const initialState = {
    isLoading: false,
    list: [], // list of ids
    byId: {},
    selectedProductsMap: new Map(),
    total: 0,
};

export default (state = initialState, {type, payload}) => {
    switch (type) {
        case FETCH_PRODUCTS:
            return {
                ...state,
                isLoading: true,
            };
        case FETCH_PRODUCTS_SUCCESS: {
            const byId = {};
            const list = [];
            const pr = payload.products.products || [];
            pr.forEach((product) => {
                if (product.unitsInStock !== 0) {
                    byId[product.productID] = product;
                    list.push(product.productID);
                }
            });
            return {
                ...state,
                byId,
                list,
            };
        }
        case FETCH_PRODUCTS_ERROR:
            return {
                ...state,
                isLoading: false,
            };
        case ADD_TO_CART: {
            let quantity = state.selectedProductsMap.get(payload.productID) || 0;
            const newlist = state.selectedProductsMap.set(payload.productID, ++quantity);
            return {
                ...state,
                selectedProductsMap: newlist,
                total: ++state.total,
            };
        }
        case REMOVE_FROM_CART: {
            let quant = state.selectedProductsMap.get(payload.productID);
            const newlisted = state.selectedProductsMap.set(payload.productID, quant <= 0 ? 0 : --quant);
            return {
                ...state,
                selectedProductsMap: newlisted,
                total: state.total <= 0 ? 0 : --state.total,
            };
        }
        case CHANGE_QUANTITY: {
            if (payload.value <= 0) {
                state.selectedProductsMap.delete(payload.productID);
            } else if (!Number.isNaN(payload.value)){
                state.selectedProductsMap.set(payload.productID, payload.value);
            }
            const arrEntries = Array.from(state.selectedProductsMap.values());
            let newTotal = 0;
            if (arrEntries.length !== 0) {
                newTotal = arrEntries.reduce((previousValue, currentValue) => previousValue + currentValue);
            }
            return {
                ...state,
                selectedProductsMap: state.selectedProductsMap,
                total: newTotal,
            };
        }
        default:
            return state;
    }
};