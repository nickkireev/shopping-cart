import {createSelector} from 'reselect';

export const selectState = state => state.app;

export const selectProductsRoot = state => selectState(state).products;

export const selectProductsIds = state => selectProductsRoot(state).byId;

export const selectProductsIsLoading = state => selectProductsRoot(state).isLoading;

export const selectProducts = createSelector(
    selectProductsRoot,
    products => products.list.map(id => products.byId[id])
);
export const selectedProductsMap = state => Array.from(selectProductsRoot(state).selectedProductsMap.entries())

export const selectedSelectProducts = createSelector(
    selectProductsRoot,
    selectedProductsMap,
    (products, map) => map.map(([productID, value]) => ({...products.byId[productID], count: value}))
);

export const selectTotal = state => selectProductsRoot(state).total;

export const selectTotalPrice = state => selectedSelectProducts(state)
    .reduce((accumulator, currentValue) => accumulator + parseFloat(currentValue.unitPrice * currentValue.count), 0);

