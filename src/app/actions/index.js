import Api from '../api';

export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_ERROR = 'FETCH_PRODUCTS_ERROR';


export const fetchProducts = async (dispatch) => {
    let products = [];
    dispatch({type: FETCH_PRODUCTS});
    try {
        products = await Api.products.get();

        dispatch({
            type: FETCH_PRODUCTS_SUCCESS,
            payload: {products},
        });
    } catch (e) {
        console.error(e);
        dispatch({type: FETCH_PRODUCTS_ERROR});
    }
};

export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const CHANGE_QUANTITY = 'CHANGE_QUANTITY';

export const addToCart = productID => ({
    type: ADD_TO_CART,
    payload: {productID},
});
export const removeFromCart = productID => ({
    type: REMOVE_FROM_CART,
    payload: {productID},
});
export const changeQuantity = (productID, value) => ({
    type: CHANGE_QUANTITY,
    payload: {productID, value},
});