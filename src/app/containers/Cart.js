import {connect} from 'react-redux';
import CartComponent from '../components/Cart/Cart';
import {selectTotalPrice, selectedSelectProducts} from '../selectors';
import {addToCart, removeFromCart} from '../actions';

const mapStateToProps = state => ({
    selectedProducts: selectedSelectProducts(state),
    total: selectTotalPrice(state),
});

const mapDispatchToProps = dispatch => ({
    onAdd: (productID) => {
        dispatch(addToCart(productID));
    },
    onRemove: (productID) => {
        dispatch(removeFromCart(productID));
    },
});

const Cart = connect(
    mapStateToProps,
    mapDispatchToProps
)(CartComponent);

export default Cart;
