import {connect} from 'react-redux';
import {addToCart} from '../actions';
import ProductComponent from '../components/Product/Product';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
    onAdd: (productID) => {
        dispatch(addToCart(productID));
    },
});

const Product = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductComponent);

export default Product;
