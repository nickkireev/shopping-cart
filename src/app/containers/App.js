import {connect} from 'react-redux';
import {selectProductsIsLoading, selectTotal} from '../selectors';
import {fetchProducts} from '../actions';
import AppComponent from '../components/App/App';

const mapStateToProps = state => ({
    isLoading: selectProductsIsLoading(state),
    total: selectTotal(state),
});

const mapDispatchToProps = dispatch => ({
    onMount: () => {
        fetchProducts(dispatch);
    },
});

const App = connect(
    mapStateToProps,
    mapDispatchToProps
)(AppComponent);

export default App;
