import {connect} from 'react-redux';
import {changeQuantity, removeFromCart} from '../actions';
import SmallProductComponent from '../components/SmallProduct/SmallProduct';

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch, props) => ({
    changeQuantity: (value) => {
        dispatch(changeQuantity(props.productID, value));
    },
    onRemove: () => {
        dispatch(removeFromCart(props.productID));
    },
});

const SmallProduct = connect(
    mapStateToProps,
    mapDispatchToProps
)(SmallProductComponent);

export default SmallProduct;
