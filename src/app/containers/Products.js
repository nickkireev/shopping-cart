import {connect} from 'react-redux';
import ProductsComponent from '../components/Products/Products';
import {selectProducts} from '../selectors';
import {addToCart, removeFromCart} from '../actions';

const mapStateToProps = state => ({
    products: selectProducts(state),
});

const mapDispatchToProps = dispatch => ({
    onAdd: (productID) => {
        dispatch(addToCart(productID));
    },
    onRemove: (productID) => {
        dispatch(removeFromCart(productID));
    },
});

const Cart = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductsComponent);

export default Cart;
