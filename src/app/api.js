export default class API {
    static async fetch(url, headers) {

        const response = await fetch(url, headers);
        const responseText = await response.text();

        try {
            return JSON.parse(responseText);
        } catch (e) {
            return responseText;
        }
    }

    static products = {
        get: () => API.fetch('https://private-3efa8-products123.apiary-mock.com/products'),
    }
}